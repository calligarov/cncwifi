/***************************************************************************
 *   Copyright (c) 2022 Vincenzo Calligaro <vincenzo.calligaro@gmail.com>  *
 *                                                                         *
 *   CNCWiFi                                                               *
 *                                                                         *
 *   This sketch is the AVR32u4 program for the project CNCWiFi            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License (LGPL)   *
 *   as published by the Free Software Foundation; either version 2 of     *
 *   the License, or (at your option) any later version.                   *
 *   for detail see the LICENCE text file.                                 *
 *                                                                         *
 *   FreeCAD is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with FreeCAD; if not, write to the Free Software        *
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
 *   USA                                                                   *
 *                                                                         *
 **************************************************************************/

#include <Bridge.h>
#include <BridgeServer.h>
#include <BridgeClient.h>

// Listen to the default port 5555, the Yún webserver
// will forward there all the HTTP requests you send
BridgeServer server(8888);

#include <Process.h>
#include <ArxContainer.h>

#include <FileIO.h>

void setup() {
  // Bridge startup
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  Bridge.begin();
  digitalWrite(13, HIGH);

  // Listen for incoming connection only from localhost
  // (no one from the external network could connect)
  server.noListenOnLocalhost();
  server.begin();

  FileSystem.begin();
}

arx::map<String, Process*> pMap;

void loop() {  
  // Get clients coming from server
  BridgeClient client = server.accept();
  
  // There is a new client?
  if (client) {
    digitalWrite(13, HIGH);
    
    // Process request
    process(client);

    // Close connection and free resources.
    client.stop();
    
    digitalWrite(13, LOW);
  }

  if (int(pMap.size())>0) {
    blink(50);

    for (const auto& pr : pMap) {
      if (!pr.second->running()){
        pMap[pr.first]->close();
        delete pMap[pr.first];
        pMap.erase(pr.first);
      }
    }
  }
  else blink(500);

}

void blink(int sec) {
    digitalWrite(13, HIGH);
    delay(sec);
    digitalWrite(13, LOW);
    delay(sec);
}

char jnl[] = "JobName: ";
  
String fileFromRequest(String *r) {
  
  int jnp = r->indexOf(jnl) + sizeof(jnl)-1;  
  String jn = r->substring(jnp, r->indexOf("\r\n", jnp));
  jn.trim();
  jn.replace(" ", "");
  jn.replace("(", "");
  jn.replace(")", "");
  jn = "/www/sd/CNCWiFi/" + jn;

  return jn;
}

void removeJob(char jobName[]) { while (FileSystem.exists(jobName)) FileSystem.remove(jobName); }

void storeJobs(BridgeClient *client, char jobName[]) {

  removeJob(jobName);
  
  File j = FileSystem.open(jobName, FILE_WRITE);
  while(!j);

  if (client->available()) client->read();
  while (client->available()) j.write(client->read());

  j.close();
}

void process(BridgeClient client) {

  String cmd = "";

  while (cmd.indexOf("Referer: ")==-1 || cmd.indexOf("Content-Length: ")==-1) {
    String row = client.readStringUntil('\r');
    row.trim();

    if (row.indexOf(" HTTP/1.1")!=-1)       cmd +=                       row.substring(row.indexOf("POST /") + String("POST /").length(), row.length()-String(" HTTP/1.1").length());
    if (row.startsWith("Referer: "))        cmd += " Referer: "        + row.substring(row.indexOf("Referer: ") + String("Referer: ").length(), row.length()-1);
    if (row.startsWith("Content-Length: ")) cmd += " Content-Length: " + row.substring(row.indexOf("Content-Length: ") + String("Content-Length: ").length());
  }
  
  client.println(F("HTTP/1.1 200 OK"));
  client.print(F("Access-Control-Allow-Origin: "));
  client.println(cmd.substring(cmd.indexOf("Referer: ") + sizeof("Referer: ")-1, cmd.indexOf(' ', cmd.indexOf("Referer: ") + sizeof("Referer: ")-1)));
  client.println();

  if (cmd.substring(cmd.indexOf("Content-Length: ") + sizeof("Content-Length: ")-1, cmd.indexOf(" ", cmd.indexOf("Content-Length: ") + sizeof("Content-Length: ")-1)).toInt()>0) {
    char bodyStart[5] = {'\r', '\n', '\r', '\n'};
    String s;
    while (s.indexOf(bodyStart)==-1) s += char(client.read());
    cmd += " Body: " + client.readStringUntil('\r');
    cmd.trim();
  }

  String shellCommand;
  if      (cmd.indexOf("getCNC")!=-1)     shellCommand = "ls /dev/tty*";
  else if (cmd.indexOf("getJobs")!=-1)    shellCommand = "ls /www/sd/CNCWiFi/ | grep .gcode";
  else if (cmd.indexOf("uploadJobs")!=-1) storeJobs(&client, cmd.substring(cmd.indexOf(jnl) + sizeof(jnl) - 1).c_str());
  else if (cmd.indexOf("removeJob")!=-1)  removeJob(cmd.substring(cmd.indexOf(jnl) + sizeof(jnl) - 1).c_str());
  else if (cmd.indexOf("sendJob")!=-1)    shellCommand = "python " + cmd.substring(cmd.indexOf("/dev/"), cmd.indexOf(" ", cmd.indexOf("/dev/")));

  if(cmd.indexOf("uploadJobs")==-1 && cmd.indexOf("removeJob")==-1) {    
    pMap[shellCommand] = new Process;
    
    pMap[shellCommand]->runShellCommandAsynchronously(shellCommand.indexOf("python")==-1?shellCommand:cmd.substring(cmd.indexOf("Body: ") + sizeof("Body: ")-1));
    
    while (!pMap[shellCommand]->running());

    while (pMap[shellCommand]->running() && !pMap[shellCommand]->available());

    while (pMap[shellCommand]->available()) client.write(char(pMap[shellCommand]->read()));
  }
  
  client.println();
}
