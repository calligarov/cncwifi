# CNCWiFi

## Overview

The CNCWiFi project has been developed to have a device capable of controlling a machine, such as a CNC or Laser engraver, that runs the GRBL firmware using a web browser.

## Features
* Streams a .gcode file to a machine that runs the GBRL firmware
* Web Interface
* Acts as an access point

## Installation
The project has been developed using an Arduino Yun Rev.2 board.
To install CNCWiFi, upload the sketch "CNCWifi.ino" via the wireless upload method, as described in the "TemperatureWebPanel" example of the Arduino Yun Bridge library.

A microSD card is required

Additional software/package needed (on the OpenWrt side of the Arduino Yun Rev.2):
* pyserial
* OpenWrt package "kmod-usb-serial-ch341"

## Usage
Connect the machine to the USB Host plug of the Arduino Yun Rev.2 using a USB cable

If not yet done, power up both the Arduino Yun Rev.2 and the machine

Connect to the access point hosted by the Arduino Yun, open your browser (not yet working on smartphones/tablets) and go to the address http://myYun.local/sd/CNCWifi, where myYun is the name of your Yun

The left column shows the list of available .gcode(s) that can be streamed to the machines

The right column shows the list of the machines connected.

Select the .gcode file you want to stream and the machine to with stream it and click the "Send Job" button.

## Support
for support send an email to vincenzo.calligaro@gmail.com

## Roadmap
* Stream to multiple machines at the same time
* Make the Web User Interface nicer
* Add some kind of "Report" zone in the Web Interface
* Add a "Job process logger" that writes the status of a job on a file. Use those files for the "Report" in the Web Interface

## Contributing
to contribute, send an email to vincenzo.calligaro@gmail.com

## Authors and acknowledgment
Vincenzo Calligaro <vincenzo.calligaro@gmail.com> - Main Developer & Maintainer
## License
See the LICENSE file for licensing information as it pertains to
files in this repository.

## Project status
As of 1/12/2022, it is available the v0.1 release of the CNCWiFi project.

Other version will be relased upon need/request
