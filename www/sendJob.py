#!/usr/bin/env python

# ***************************************************************************
# *   Copyright (c) 2022 Vincenzo Calligaro <vincenzo.calligaro@gmail.com>  *
# *                                                                         *
# *   sendJob.py                                                            *
# *                                                                         *
# *   This python script is used to decouple the python process             *
# *   "simple_stream.py" from the process called by the AVR32u4 processor   *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Library General Public License (LGPL)   *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   FreeCAD is distributed in the hope that it will be useful,            *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with FreeCAD; if not, write to the Free Software        *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

import sys, subprocess

print(sys.argv[1] + " " + sys.argv[2])

subprocess.run(["python /www/sd/CNCWiFi/simple_stream.py ", sys.argv[1] + " ", sys.argv[2]])
